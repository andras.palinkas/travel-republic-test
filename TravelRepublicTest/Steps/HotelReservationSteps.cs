﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Assist;
using TravelRepublicTest.Models;
using TravelRepublicTest.Pages;
using Xunit;

namespace TravelRepublicTest.Steps
{
    [Binding]
    public class HotelReservationSteps
    {
        private IWebDriver driver;
        private HotelReservation hotelReservation;
        private HomePage home;
        private HotelResultsPage hotelResults;

        [Given(@"I open Google Chrome")]
        public void GivenIOpenGoogleChrome()
        {
            driver = new ChromeDriver();
            driver.Manage().Window.Maximize();
        }

        [AfterScenario]
        public void TearDownFeature()
        {
            driver.Dispose();
            driver.Quit();
        }

        [Given(@"I navigate to Main Page")]
        public void GivenINavigateToMainPage()
        {
            home = new HomePage(driver);
            home.GoToPage();
        }

        [Then(@"Main Page is loaded")]
        public void ThenMainPageIsLoaded()
        {
            home.WaitForLoad();
            Assert.True(home.IsLoaded());
        }

        [When(@"I click on Hotels tab")]
        public void WhenIClickOnHotelsButton()
        {
            home.ClickOnHotels();
        }


        [When(@"I select Budapest city")]
        public void WhenISelectBudapestCity()
        {
            home.FillHotelFieldWith("Budapest");
        }

        [Given(@"I make a hotel search with following parameters")]
        public void GivenIMakeAHotelSearchWithFollowingParameters(Table table)
        {
            hotelReservation = new HotelReservation();
            dynamic attributes = table.CreateDynamicInstance();

            hotelReservation.WhereToGo = attributes.city;
            hotelReservation.StartDate = attributes.startDate != null ? DateTime.Parse(attributes.startDate) : null;
            hotelReservation.EndDate = attributes.endDate != null ? DateTime.Parse(attributes.endDate) : null;
            hotelReservation.AddRoom(new Room(attributes.adults, attributes.children));
            hotelReservation.Currency = attributes.currency != null ? attributes.currency : "EUR";
            hotelReservation.HaveDate = attributes.startDate != null || attributes.endDate != null ? false : true;

            home.HotelSearch(hotelReservation);
        }

        [When(@"I click on Search")]
        public void WhenIClickOnSearch()
        {
            // TODO Click on search button
        }

        [Then(@"Hotel page is loaded")]
        public void ThenHotelPageIsLoaded()
        {
            Assert.True(hotelResults.IsLoaded());
        }

        [When(@"I select (.*) city")]
        public void WhenISelectCity(string city)
        {
            home.SetDestination(city);
        }

        [When(@"I check the don't have date yet checkbox")]
        public void WhenICheckTheDonTHaveDateYetCheckbox()
        {
            home.CheckDontHaveDateCheckBox();
        }

        [Then(@"The date field is disabled")]
        public void ThenTheDateFieldIsDisabled()
        {
            //TODO
        }


    }
}
