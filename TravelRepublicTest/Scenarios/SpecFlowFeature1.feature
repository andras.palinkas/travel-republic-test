﻿Feature: HotelReservation
	In order to reserve a hotel room
	As a User, who want to live somewhere
	I want a reserved hotel room

Background: 
	Given I open Google Chrome

Scenario: Navigate to main page
	Given I navigate to Main Page
	Then Main Page is loaded

Scenario: Hotel reservation
	Given I make a hotel search with following parameters
	| city     | startDate   | endDate    | adults | children | currency |
	| Budapest | 2018.03.01. | 2018.03.05 | 1      | 0        | HUF      |
	| London   |             |            | 2      | 2        | EUR      |
	When I click on Search
	Then Hotel page is loaded



Scenario: Hotel reservarion w/o date
	When I select <city> city
	| city     |
	| Budapest |
		And I check the don't have date yet checkbox
	Then The date field is disabled