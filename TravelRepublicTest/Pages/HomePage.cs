﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using System.Configuration;
using OpenQA.Selenium.Support.UI;
using TravelRepublicTest.Models;

namespace TravelRepublicTest.Pages
{
    class HomePage
    {
        private IWebDriver driver;
        private RoomServePage roomServe;

        public HomePage(IWebDriver driver)
        {
            this.driver = driver;
            PageFactory.InitElements(driver, this);
        }

        public void GoToPage()
        {   try
            {
                driver.Navigate().GoToUrl(ConfigurationManager.AppSettings["MAINURL"]);
            } catch(Exception e)
            {
                throw e;
            }
        }

        public void WaitForLoad()
        {
            new WebDriverWait(driver, TimeSpan.FromSeconds(10)).Until(
                d => ((IJavaScriptExecutor)d).ExecuteScript("return document.readyState").Equals("complete"));
        }

        public void ClickOnHotels()
        {
            driver.FindElement(By.CssSelector(".icon-hotels")).Click();
        }

        public void FillHotelFieldWith(String hotel)
        {
            // TODO write in hotels input field
        }

        public void SetDestination(string destination)
        {
            //TODO fill destination
        }


        public void HotelSearch(HotelReservation hotelReservation)
        {
            SetDestination(hotelReservation.WhereToGo);
            // TODO set have dates yet
            // TODO fill start date
            // TODO fill end date
            roomServe.AddRooms(hotelReservation.WhoIsGoing);
            // TODO set currency
        }

        public Boolean IsLoaded()
        {
            if (driver.FindElement(By.CssSelector(".search-unit-field-row")) != null)
            {
                return true;
            }
            return false;
        }

        public void CheckDontHaveDateCheckBox()
        {
            //TODO
        }
    }
}
