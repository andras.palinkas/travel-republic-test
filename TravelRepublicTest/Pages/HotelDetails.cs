﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TravelRepublicTest.Models;

namespace TravelRepublicTest.Pages
{
    class HotelDetails
    {
        private IWebDriver driver;

        public HotelDetails(IWebDriver driver)
        {
            this.driver = driver;
            PageFactory.InitElements(driver, this);
        }

        public Boolean IsLoaded()
        {
            //TODO check page is loaded
            return false;
        }

        public IList<RoomType> GetRooms()
        {
            IList<RoomType> rooms = new List<RoomType>();
            //TODO
            return rooms;
        }

        public void SelectRoom()
        {
            //TODO
        }
    }
}
