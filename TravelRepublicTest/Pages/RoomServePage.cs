﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TravelRepublicTest.Models;

namespace TravelRepublicTest.Pages
{
    class RoomServePage
    {
        private IWebDriver driver;

        public RoomServePage(IWebDriver driver)
        {
            this.driver = driver;
            PageFactory.InitElements(driver, this);
        }

        public void SetAdultsNumber(int roomCtr, int adultsNumber)
        {
            //TODO
        }

        public void SetChildrenNumber(int roomCtr, int childrenNumbe)
        {
            //TODO
        }

        public void AddRoom(Room room)
        {
            //TODO click add room
            //TODO set Adults number
            //TODO set Children number
        }

        public void AddRooms(IList<Room> rooms)
        {
            foreach (Room room in rooms)
            {
                this.AddRoom(room);
            }
        }
    }
}
