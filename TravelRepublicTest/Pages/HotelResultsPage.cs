﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TravelRepublicTest.Models;

namespace TravelRepublicTest.Pages
{
    class HotelResultsPage
    {
        private IWebDriver driver;

        public HotelResultsPage(IWebDriver driver)
        {
            this.driver = driver;
            PageFactory.InitElements(driver, this);
        }

        public Boolean IsLoaded()
        {
            //TODO check page is loaded
            return false;
        }

        public IList<Hotel> GetHotelResultsFromCurrentPage()
        {
            IList<Hotel> hotelResults = new List<Hotel>();

            //TODO
        
            return hotelResults;
        }

        public IList<Hotel> GetHotelResults(int pageNumber)
        {
            GoToPage(pageNumber);
            return GetHotelResultsFromCurrentPage();
        }

        public void GoToPage(int pageNumber)
        {
            // TODO click on page number if visible
        }

        public void GoToHotelDetails(int hotelNumber)
        {
            //TODO click on correct hotel's view deatils
        }

        public void GoToHotelDetails(string hotelName)
        {
            //TODO click on correct hotel's view deatils
        }
    }
}
