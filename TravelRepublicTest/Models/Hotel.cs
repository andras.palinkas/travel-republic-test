﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TravelRepublicTest.Models
{
    class Hotel
    {
        public string Title { get; set; }
        public string Location { get; set; }
        public double Stars { get; set; }
        public double Rating { get; set; }
        public double PriceFrom { get; set; }
        public string PriceCurrency { get; set; }
    }
}
