﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TravelRepublicTest.Models
{
    class HotelReservation
    {
        public string WhereToGo { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public Boolean HaveDate { get; set; }
        public string Currency { get; set; }

        private IList<Room> whoIsGoing;

        public IList<Room> WhoIsGoing
        {
            get { return whoIsGoing; }
        }

        public void AddRoom(Room room)
        {
            whoIsGoing.Add(room);
        }

        public void AddRoomRange(IList<Room> rooms)
        {
            foreach (var room in rooms)
            {
                whoIsGoing.Add(room);
            }
        }


    }
}
