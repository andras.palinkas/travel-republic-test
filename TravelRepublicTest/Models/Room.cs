﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TravelRepublicTest.Models
{
    class Room
    {
        public int AdultsNumber { get; set; }
        public int ChildrenNumber { get; set; }

        public Room(int adults, int children)
        {
            AdultsNumber = adults;
            ChildrenNumber = children;
        }


    }
}
